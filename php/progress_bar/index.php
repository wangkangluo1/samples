<?php
/**
 * Created by JetBrains PhpStorm.
 * User: m
 * Date: 13-9-13
 * Time: 上午10:31
 * To change this template use File | Settings | File Templates.
 */
// Generate random string for our upload identifier
$uid = md5(uniqid(mt_rand()));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Upload Something</title>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/start/jquery-ui.css" rel="stylesheet" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js"></script>
    <script src="./ajaxfileupload.js"></script>
    <style>
        #progress-bar, #upload-frame {
            display: none;
        }
    </style>
    <script>
        // JavaScript here
        /**
         * Created with JetBrains PhpStorm.
         * User: m
         * Date: 13-9-13
         * Time: 上午10:35
         * To change this template use File | Settings | File Templates.
         */
        // We'll use this to cache the progress bar node
        var pbar;

        // This flag determines if the upload has started
        var started = false;


        function updateProgress(id) {

            var time = new Date().getTime();

            // Make a GET request to the server
            // Pass our upload identifier as a parameter
            // Also pass current time to prevent caching
            $.get('getprogress.php', { uid: id, t: time }, function (data) {
                // Get the output as an integer
                var progress = parseInt(data, 10);

                if (progress < 100 || !started) {

                    // Determine if upload has started
                    started = progress < 100;

                    // If we aren't done or started, update again
                    updateProgress(id);

                }

                // Update the progress bar percentage
                // But only if we have started
                started && pbar.progressbar('value', progress);
            });

        }

        $(function () {

            // Start progress tracking when the form is submitted
            $('#submit_button').click(function() {

                $.ajaxFileUpload
                (
                    {
                        url: './upload.php',
                        secureuri:false,
                        fileElementId:'file_upload',
                        dataType: 'json',
                        data:{
                            UPLOAD_IDENTIFIER: '<?php echo $uid; ?>'
                        },
                        success: function (data, status)
                        {
                            $("#logo").attr("src", data.logo_url);
                            if(typeof(data.error) != 'undefined')
                            {
                                if(data.error != '')
                                {
                                    alert(data.error);
                                }else
                                {
                                    $('#notify').html(data.msg + '!');
                                    var time_id = setInterval(function(){
                                        $("#notify").fadeOut(150).fadeIn(150);
                                    },200);
                                    setTimeout(function() {
                                        clearInterval(time_id);
                                        $('#notify').fadeOut();
                                    }, 1300);

                                }
                            }
                        },
                        error: function (data, status, e)
                        {
                            alert(e);
                        }
                    }
                )

                // Hide the form
                $('#upload-form').hide();

                // Cache the progress bar
                pbar = $('#progress-bar');

                // Show the progress bar
                // Initialize the jQuery UI plugin
                pbar.show().progressbar();

                // We know the upload is complete when the frame loads
                $('#upload-frame').load(function () {

                    // This is to prevent infinite loop
                    // in case the upload is too fast
                    started = true;

                    // Do whatever you want when upload is complete
                    alert('Upload Complete!');

                });

                // Start updating progress after a 1 second delay
                setTimeout(function () {

                    // We pass the upload identifier to our function
                    updateProgress($('#uid').val());

                }, 100);

            });



        });
    </script>
</head>
<body>
<!--
<form id="upload-form"
      method="post"
      action="upload.php"
      enctype="multipart/form-data"
      target="upload-frame" > -->
    <input type="hidden"
           id="uid"
           name="UPLOAD_IDENTIFIER"
           value="<?php echo $uid; ?>" >
    <input id="file_upload" type="file" name="file">
    <input id="submit_button" type="submit" name="submit" value="Upload!">
<!--</form>-->
<div id="progress-bar"></div>
<iframe id="upload-frame" name="upload-frame"></iframe>
</body>
</html>